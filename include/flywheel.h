
#define flywheel_KP 0.8
#define flywheel_KI 0.055
#define flywheel_KD 0.0075

// Maximum motor power
#define MAX_POWER 127

int fly_error = 0;
int fly_integral = 0;
int fly_derivative = 0;
int fly_lastError = 0;
float fly_avg = 0;
int fly_num = 0;
bool fly_reset = false;

void flywheelPID(int TARGET_VELOCITY) {
  fly_error = TARGET_VELOCITY - flywheel.get_actual_velocity();
  fly_integral += fly_error;
  fly_derivative = fly_error-fly_lastError;
  fly_lastError = fly_error;
  int fly_power = flywheel_KP*fly_error + flywheel_KI*fly_integral + flywheel_KD*fly_derivative;
  fly_num = fly_num + 1;
  fly_avg = (((flywheel.get_actual_velocity()-200)/200)+fly_num*fly_avg)/fly_num;
  flywheel.move_velocity(fly_power);
  
}