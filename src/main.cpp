#include "main.h"
#include "EZ-Template/util.hpp"
#include "autons.hpp"
#include "pros/colors.h"
#include "pros/llemu.hpp"
#include "pros/misc.h"
#include "pros/motors.h"
#include "pros/motors.hpp"
#include "pros/rtos.h"
#include "pros/rtos.hpp"
#include "pros/screen.h"
#include "pros/screen.hpp"
#include <cmath>
#include <cstdio>
#include <vector>


/////
// For instalattion, upgrading, documentations and tutorials, check out website!
// https://ez-robotics.github.io/EZ-Template/
/////

bool wingBool = false;
bool liftBool = false;
bool puncherOn = false;
double position = 0;
//double flyspeed = -130000;
pros::Motor puncher (7);
pros::Motor intake (16);
pros::ADIDigitalOut liftOneWay (3);
pros::ADIDigitalOut liftTwoWay (4);
pros::ADIDigitalOut wings (2);
pros::IMU imu (21);
//pros::Motor_Group ALL_MOTORS ({chassis.left_motors[0], chassis.left_motors[1], chassis.left_motors[2],chassis.right_motors[0],chassis.right_motors[1], chassis.right_motors[2], intake, puncher});
/**
 * Dead Ports: 8 (Half), 3 (Dead)
*/
vector<int> lm1{-4,-13, -14}; //-13
vector<int> rm1{5,6, 9}; // 9


// Chassis constructor
Drive chassis (
  // Left Chassis Ports (negative port will reverse it!)
  //   the first port is the sensored port (when trackers are not used!)
  lm1

  // Right Chassis Ports (negative port will reverse it!)
  //   the first port is the sensored port (when trackers are not used!)
  ,rm1

  // IMU Port
  ,21

  // Wheel Diameter (Remember, 4" wheels are actually 4.125!)
  //    (or tracking wheel diameter)
  ,3.25

  // Cartridge RPM
  //   (or tick per rotation if using tracking wheels)
  ,600

  // External Gear Ratio (MUST BE DECIMAL)
  //    (or gear ratio of tracking wheel)
  // eg. if your drive is 84:36 where the 36t is powered, your RATIO would be 2.333.
  // eg. if your drive is 36:60 where the 60t is powered, your RATIO would be 0.6.
  ,1.667

  // Uncomment if using tracking wheels
  /*
  // Left Tracking Wheel Ports (negative port will reverse it!)
  // ,{1, 2} // 3 wire encoder
  // ,8 // Rotation sensor

  // Right Tracking Wheel Ports (negative port will reverse it!)
  // ,{-3, -4} // 3 wire encoder
  // ,-9 // Rotation sensor
  */

  // Uncomment if tracking wheels are plugged into a 3 wire expander
  // 3 Wire Port Expander Smart Port
  // ,1
);

   

/**
 * Runs initialization code. This occurs as soon as the program is started.
 *
 * All other competition modes are blocked by initialize; it is recommended
 * to keep execution time for this mode under a few seconds.
 */

void initialize() {
  pros::lcd::initialize();
  pros::lcd::set_background_color(26,26,26);
  pros::lcd::set_text_color(162,0,236);
  // Print our branding over your terminal :D
  chassis.imu_calibrate();
  pros::delay(50); // Stop the user from doing anything while legacy ports configure.
  // Configure your chassis controls
  chassis.toggle_modify_curve_with_controller(true);
  master.rumble("- ."); // Enables modifying the controller curve with buttons on the joysticks
  chassis.set_active_brake(0.075); // Sets the active brake kP. We recommend 0.1.
  chassis.set_curve_default(4,4); // Defaults for curve. If using tank, only the first parameter is used. (Comment this line out if you have an SD card!)  
  default_constants(); // Set the drive to your own constants from autons.cpp!
  exit_condition_defaults(); // Set the exit conditions to your own constants from autons.cpp!
  
  // These are already defaulted to these buttons, but you can change the left/right curve buttons here!
  // chassis.set_left_curve_buttons (pros::E_CONTROLLER_DIGITAL_LEFT, pros::E_CONTROLLER_DIGITAL_RIGHT); // If using tank, only the left side is used. 
  // chassis.set_right_curve_buttons(pros::E_CONTROLLER_DIGITAL_Y,    pros::E_CONTROLLER_DIGITAL_A);

  // Autonomous Selector using LLEMU
  ez::as::auton_selector.add_autons({
    Auton("Other Side", otherSide),
    Auton("Match Load Auton", matchLoad),
    Auton("Skills", skills),
    Auton("No Auton", no_auton),
  });


  // Initialize chassis and auton selector
  chassis.initialize();
  ez::as::initialize();
}



/**
 * Runs while the robot is in the disabled state of Field Management System or
 * the VEX Competition Switch, following either autonomous or opcontrol. When
 * the robot is enabled, this task will exit.
 */
void disabled() {
  // . . .
}



/**
 * Ishaan says "I hate Juice"
 * Runs after initialize(), and before autonomous when connected to the Field
 * Management System or the VEX Competition Switch. This is intended for
 * competition-specific initialization routines, such as an autonomous selector
 * on the LCD.
 *
 * This task will exit when the robot is enabled and autonomous or opcontrol
 * starts.
 */
void competition_initialize() {
  // . . .
  

}

/**
 * Runs the user autonomous code. This function will be started in its own task
 * with the default priority and stack size whenever the robot is enabled via
 * the Field Management System or the VEX Competition Switch in the autonomous
 * mode. Alternatively, this function may be called in initialize or opcontrol
 * for non-competition testing purposes.
 *
 * If the robot is disabled or communications is lost, the autonomous task
 * will be stopped. Re-enabling the robot will restart the task, not re-start it
 * from where it left off.
 */
void autonomous() {

  chassis.reset_pid_targets(); // Resets PID targets to 0
  chassis.reset_gyro(); // Reset gyro position to 0
  chassis.reset_drive_sensor(); // Reset drive sensors to 0
  chassis.set_drive_brake(MOTOR_BRAKE_HOLD); // Set motors to hold.  This helps autonomous consistency.
  pros::lcd::print(3, "Heading %3.2f\n", imu.get_heading());
  ez::as::auton_selector.call_selected_auton(); // Calls selected auton from autonomous selector.

}



/**
 * Runs the operator control code. This function will be started in its own task
 * with the default priority and stack size whenever the robot is enabled via
 * the Field Management System or the VEX Competition Switch in the operator
 * control mode.
 *
 * If no competition control is connected, this function will run immediately
 * following initialize().x
 * aayush was here
 *
 * If the robot is disabled or communications is lost, the
 * operator control task will be stopped. Re-enabling the robot will restart the
 * task, not resume it from where it left off.
 */

void opcontrol() {
  intake.set_brake_mode(pros::E_MOTOR_BRAKE_COAST);
  int count = 0;
   puncher.set_gearing(pros::E_MOTOR_GEAR_600);
    int start;
  while (true) {
    chassis.tank();
    ///INTAKE CODE
      //Intake
    if(master.get_digital(DIGITAL_R1)){
      intake.move_velocity(-200);
    }
      //Outtake
    if(master.get_digital(DIGITAL_R2)){
      intake.move_velocity(200);
    }
      //AUTO-STOP
    if((master.get_digital(DIGITAL_R2) && master.get_digital(DIGITAL_R1))==false){
      intake.move_velocity(0);
    }
    ///WINGS CODE
    if(master.get_digital_new_press(DIGITAL_X)){
      wingBool = !wingBool;
      wings.set_value(wingBool);
      pros::delay(20);
    }
    if(master.get_digital_new_press(DIGITAL_Y)){
      liftOneWay.set_value(!liftBool);
      liftBool = !liftBool;
      pros::delay(20);
    }
    ///puncher Code
    // if(master.get_digital_new_press(DIGITAL_A)){
    //   if(puncherOn){
    //     puncherOn = false;
    //     puncher.move_relative(10, 100);
    //     // position = puncher.get_position();
    //     // if(position != 0){
    //     //   position = 100-position;
    //     // }
    //     pros::delay(50);
    //   }
    //   else{
    //     puncherOn = true;
    //     pros::delay(50);

    //   }
    // }
    if(master.get_digital(DIGITAL_A)){
      puncher.move_velocity(100);
    }
    if(!master.get_digital(DIGITAL_A)){
      puncher.move_velocity(0);
    }
    double avg_temp = (chassis.left_motors[0].get_temperature()+chassis.left_motors[1].get_temperature()+chassis.left_motors[2].get_temperature()+chassis.right_motors[0].get_temperature()+chassis.right_motors[1].get_temperature()+chassis.right_motors[2].get_temperature()+intake.get_temperature()+puncher.get_temperature())/8;
    master.print(0,1,"FLYSPEED: %d\n", puncher.get_actual_velocity()*4);
    pros::delay(ez::util::DELAY_TIME);
  }
  chassis.set_joystick_threshold(9);
}
