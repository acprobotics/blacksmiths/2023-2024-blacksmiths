#include "autons.hpp"
#include "EZ-Template/drive/drive.hpp"
#include "EZ-Template/sdcard.hpp"
#include "EZ-Template/util.hpp"
#include "main.h"
#include "pros/motors.hpp"
#include "pros/rtos.hpp"
#include <cstdio>

/////
// For instalattion, upgrading, documentations and tutorials, check out website!
// https://ez-robotics.github.io/EZ-Template/
/////

extern bool wingBool;
extern bool liftBool;
extern pros::Motor intake;
extern pros::ADIDigitalOut liftOneWay;
extern pros::ADIDigitalOut liftTwoWay;
extern pros::ADIDigitalOut wings;
extern pros::IMU imu;
extern pros::Motor puncher;
const int DRIVE_SPEED = 112; // This is 110/127 (around 87% of max speed).  We don't suggest making this 127.
                             // If this is 127 and the robot tries to heading correct, it's only correcting by
                             // making one side slower.  When this is 87%, it's correcting by making one side
                             // faster and one side slower, giving better heading correction.
const int TURN_SPEED  = 90;
const int SWING_SPEED = 105;



///
// Constants
///

// It's best practice to tune constants when the robot is empty and with heavier game objects, or with lifts up vs down.
// If the objects are light or the cog doesn't change much, then there isn't a concern here.
inline void wd(){
  chassis.wait_drive();
}
inline void intakeRun(int speed, int time){
  intake.move_voltage(speed);
  pros::delay(time);
  intake.move_voltage(0);
}

inline void turn(float angle,int speed = TURN_SPEED,  bool holding = true){
  chassis.set_turn_pid(angle, speed);
  if (holding) wd();
}
inline void drive(float dist, int speed = DRIVE_SPEED, bool holding= true, bool slew = false){
  chassis.set_drive_pid(dist, speed, slew, true);
  if (holding) wd();
}

void default_constants() {
  chassis.set_slew_min_power(80, 80);
  chassis.set_slew_distance(7, 7);
  chassis.set_pid_constants(&chassis.headingPID, 11, 0, 20, 0);
  chassis.set_pid_constants(&chassis.forward_drivePID, 0.45, 0, 5, 0);
  chassis.set_pid_constants(&chassis.backward_drivePID, 0.45, 0, 5, 0);
  chassis.set_pid_constants(&chassis.turnPID, 5, 0.003, 35, 15);
  chassis.set_pid_constants(&chassis.swingPID, 7, 0, 45, 0);
}

void one_mogo_constants() {
  chassis.set_slew_min_power(80, 80);
  chassis.set_slew_distance(7, 7);
  chassis.set_pid_constants(&chassis.headingPID, 11, 0, 20, 0);
  chassis.set_pid_constants(&chassis.forward_drivePID, 0.45, 0, 5, 0);
  chassis.set_pid_constants(&chassis.backward_drivePID, 0.45, 0, 5, 0);
  chassis.set_pid_constants(&chassis.turnPID, 5, 0.003, 35, 15);
  chassis.set_pid_constants(&chassis.swingPID, 7, 0, 45, 0);
}

void two_mogo_constants() {
  chassis.set_slew_min_power(80, 80);
  chassis.set_slew_distance(7, 7);
  chassis.set_pid_constants(&chassis.headingPID, 11, 0, 20, 0);
  chassis.set_pid_constants(&chassis.forward_drivePID, 0.45, 0, 5, 0);
  chassis.set_pid_constants(&chassis.backward_drivePID, 0.45, 0, 5, 0);
  chassis.set_pid_constants(&chassis.turnPID, 5, 0.003, 35, 15);
  chassis.set_pid_constants(&chassis.swingPID, 7, 0, 45, 0);
}

void exit_condition_defaults() {
  chassis.set_exit_condition(chassis.turn_exit, 100, 3, 500, 7, 500, 500);
  chassis.set_exit_condition(chassis.swing_exit, 100, 3, 500, 7, 500, 500);
  chassis.set_exit_condition(chassis.drive_exit, 80, 50, 300, 150, 500, 500);
}

void modified_exit_condition() {
  chassis.set_exit_condition(chassis.turn_exit, 100, 3, 500, 7, 500, 500);
  chassis.set_exit_condition(chassis.swing_exit, 100, 3, 500, 7, 500, 500);
  chassis.set_exit_condition(chassis.drive_exit, 80, 50, 300, 150, 500, 500);
}



////////AUTONS

void no_auton() {
  drive(24);
}
void otherSide(){
  ////SET HEADING INITIAL FOR CONSISTENCY
  imu.set_heading(0);
  ////INTAKE TRIBALL UNDER HANG BAR
  drive(3);
  intakeRun(-13000, 350);
  
  ///PUSH PRELOAD INTO GOAL
    //MOVE BACK TO MATCHLOAD ZONE
  chassis.set_drive_pid(-42, DRIVE_SPEED, false, false);
  chassis.wait_until(-32);
    //TURN AT MATCHLOAD
  turn(-45, TURN_SPEED);
    //DRIVE AT MATCHLOAD ZONE
  drive(-20, DRIVE_SPEED);
    //TURN TO GOAL
  turn(-90, TURN_SPEED);
    //PUSH PRELOAD INTO GOAL
  drive(-11, 127, true);
    //DRIVE OUT OF GOAL
  drive(10, DRIVE_SPEED);
  ///1ST TRIBALL
    //TURN TO LEAVE GOAL
  turn(5, TURN_SPEED);
    //DRIVE OUT
  drive(28, DRIVE_SPEED);
    //TURN TO GOAL
  turn(110, TURN_SPEED);
  drive(20);
  intake.move_voltage(13000);
  pros::delay(250);
  intake.move_voltage(0);
  drive(-16);
  ///2ND TRIBALL
    //TURN TO 2ND TRIBALL
  turn(30);
    //DRIVE TO TRIBALL
  drive(28, DRIVE_SPEED);
    //INTAKE TRIBALL
  intake.move_voltage(-13000);
  pros::delay(250);
  intake.move_voltage(0);
    //TURN TO GOAL
  turn(165, TURN_SPEED);
    //DRIVE CLOSER TO GOAL
  drive(24, DRIVE_SPEED);
    //OUTTAKE
  intake.move_voltage(13000);
  pros::delay(200);
  intake.move_voltage(0);
  ///3RD TRIBALL
  drive(-11);
    //TURN TO TRIBALL
  turn(90);
  drive(12);
    //INTAKE TRIBALL
  intake.move_voltage(-13000);
  pros::delay(200);
  intake.move_voltage(0);
    //TURN TO GOAL
  turn(190);
    //DRIVE TO 
  drive(5, 137);
  intake.move_voltage(13000);
  pros::delay(100);
  intake.move_voltage(0);
  drive(-5, 127);
  ///FOURTH TRIBALL
    //TURN AROUND
  turn(25);
    //DRIVE TO TRIBALL
  drive(20, 127);

    //INTAKE
    intake.move_voltage(-13000);
  pros::delay(200);
  intake.move_voltage(0);
  drive(-5);
    //TURN AROUND TO GOAL
  turn(190);

    //OUTTAKE
  intake.move_voltage(13000);
  drive(30, 137);
  pros::delay(100);
  intake.move_voltage(0);
  drive(-10);


}
void matchLoad() {
  
  //SET HEADING FOR CONSISTENCY
  imu.set_heading(0);
  ///PRELOAD
    //INTAKE PRELOAD
  
  ////2ND TRIBALL
    ///DRIVE TO TRIBALL
      //INITIAL DRIVE
  chassis.set_drive_pid(54.5, 127, true, true);
      //WAIT UNTIL A CERTAIN POINT
  chassis.wait_until(40);
  intake.move_voltage(-13000);
  wd();
  pros::delay(200);
  intake.move_voltage(0);
  turn(65);
  wingBool = !wingBool;
  wings.set_value(wingBool);
  drive(28, 127);
  intake.move_voltage(13000);
  pros::delay(1000);
  intake.move_voltage(0);



}


void halfAWP(){

}

void skills() {
  puncher.move(127);
  pros::delay(2000);
  puncher.move(0);
}

